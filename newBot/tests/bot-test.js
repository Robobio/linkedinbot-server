var chai = require("chai");

var chaiAsPromised = require('chai-as-promised');
chai.use(chaiAsPromised);

var expect = chai.expect; // we are using the "expect" style of Chai
var Firebase = require("../FirebaseRef");
var MockFirebase = require('mockfirebase').MockFirebase;
var Bot = require("../bot.js");

var campaignFixture = {
  "account" : "",
  "campaignname" : "codeigniter php mysql",
  "company" : "",
  "companysize" : "",
  "connectionlevel" : "",
  "countryCode" : "",
  "currentcompany" : "",
  "credentials" : "-KPwpdku4QqfkUGq9_dX",
  "distance" : "",
  "duration" : 604800000,
  "firstName" : "",
  "function" : "",
  "groups" : "",
  "industry" : "",
  "interestedin" : "",
  "keywords" : "codeigniter php mysql",
  "lastName" : "",
  "locationType" : "",
  "notification" : "Getting profiles list",
  "postalCode" : "",
  "resultCount" : "85.139",
  "school" : "",
  "seniority" : "",
  "status" : "notready",
  "title" : "",
  "titleScope" : "CP",
  "user" : "gMeMJHEWZxMbzlKaehLuWxnKynz2",
  "velocity" : 7103,
  "visitingStatus" : "ready",
  "visitingconnections" : "yes"
};

var credentialsFixture = {
  "gMeMJHEWZxMbzlKaehLuWxnKynz2" : {
    "-KPwpdku4QqfkUGq9_dX" : {
      "country" : "AR",
      "password" : "juanpedro123456",
      "uid" : "gMeMJHEWZxMbzlKaehLuWxnKynz2",
      "username" : "juanpedro@robob.io"
    }
  },
  "vqWfw5OdMIa1QiAUeGss5isHp8v2" : {
    "-KPmcIIVsD1vC91YOwaA" : {
      "country" : "US",
      "password" : "TA13zo9fh4U1",
      "uid" : "vqWfw5OdMIa1QiAUeGss5isHp8v2",
      "username" : "andrew.bernstein00@gmail.com"
    }
  }
};

var myCredentialFixture = {
  "country" : "AR",
  "password" : "juanpedro123456",
  "uid" : "gMeMJHEWZxMbzlKaehLuWxnKynz2",
  "username" : "juanpedro@robob.io"
};



describe("Bot", function(){
  it('Bot(Objeto) deberia devolver una copia de Objeto', function(){
    var botPromise = Bot.init(campaignFixture);
    return expect(botPromise).to.eventually.equal(campaignFixture);
  });

  it("Deberia conseguir las credenciales del usuario que va a impersonar", function(){
    var fb = new MockFirebase('mock://eg.firebaseio.com/credentials'); // loads the default data
    fb.set(credentialsFixture);
    fb.flush();
    var credencialesPromise = Bot.getCredentials(campaignFixture);
    // credencialesPromise.then(function(result){
    //   console.log(result);
    //   console.log(myCredentialFixture);
    // });
    return expect(credencialesPromise).to.eventually.deep.equal(myCredentialFixture);
  });

  it('Conseguir un proxy disponible para usar en esta campana', function(){
    var proxyPromise = Bot.getProxyForCampaign(campaignFixture);
    return expect(proxyPromise).to.eventually.equal(true);
  });



});
