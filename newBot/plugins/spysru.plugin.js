var Promise = require('promise');
var pc = require("proxy-checker");

var listIndex = 4;

var processResult = function(result){
  return result;
};

var checkProxys = function(hostPort){
  return new Promise(
    function(resolve, reject) {
      console.log("Checking Proxy..."+hostPort);
      var arr = hostPort.split(":");
      pc.checkProxy(arr[0], arr[1], {
        // the complete URL to check the proxy
        url: 'http://www.example.com',
        // an optional regex to check for the presence of some text on the page
        regex: /Example Domain/
      }, function(host, port, ok, statusCode, err) {
        if (ok) {
          console.log("Proxy OK", host, port);
          resolve(host +":"+ port);
        }else{
          console.log("Proxy Failed", host, port);
          reject();
        }
        //console.log(host + ':' + port + ' => ' + ok + ' (status: ' + statusCode + ', err: ' + err + ')');
      });
    }
  );
};

var getProxy = function(countryCode){
  countryCode = countryCode.toUpperCase();
  return new Promise(
    function(resolve, reject) {
      var Nightmare = require('nightmare');
      var nightmare = Nightmare({show: true});
      nightmare
        .goto('http://spys.ru/free-proxy-list/' + countryCode + '/')
        .evaluate(function () {
          var proxyList = [];
          proxyList.push(document.querySelector("body > table:nth-child(3) > tbody > tr:nth-child(4) > td > table > tbody > tr:nth-child(4) > td:nth-child(1) > font.spy14").innerText);
          proxyList.push(document.querySelector("body > table:nth-child(3) > tbody > tr:nth-child(4) > td > table > tbody > tr:nth-child(5) > td:nth-child(1) > font.spy14").innerText);
          proxyList.push(document.querySelector("body > table:nth-child(3) > tbody > tr:nth-child(4) > td > table > tbody > tr:nth-child(6) > td:nth-child(1) > font.spy14").innerText);
          proxyList.push(document.querySelector("body > table:nth-child(3) > tbody > tr:nth-child(4) > td > table > tbody > tr:nth-child(7) > td:nth-child(1) > font.spy14").innerText);
          proxyList.push(document.querySelector("body > table:nth-child(3) > tbody > tr:nth-child(4) > td > table > tbody > tr:nth-child(8) > td:nth-child(1) > font.spy14").innerText);
          return proxyList;
        })
        .end()
        .then(function (hostPort) {
          console.log(hostPort);
          var i = 0;
          var resolved = false;

          var success = function(hostPort){
            if(!resolved){
              resolved = true;
              resolve(hostPort);
            }
            return true;
          };

          var errorFn = function(error){
            console.log(error);
          };

          while(i<5){
            checkProxys(hostPort[i]).then(success).catch(errorFn);
            i++;
          }

        })
        .catch(function (error) {
          reject(error);
        });
    }
  );
};



module.exports = {
  getProxy: getProxy
};
