var Firebase = require("./FirebaseRef");
var Promise = require('promise');
var Bot = require("./bot");

var interval = function (func, wait, times) {
    var interv = function (w, t) {
        return function () {
            if (typeof t === "undefined" || t-- > 0) {
                setTimeout(interv, w);
                try {
                    func.call(null);
                }
                catch (e) {
                    t = 0;
                    throw e.toString();
                }
            }
        };
    }(wait, times);
    setTimeout(interv, wait);
};

var checkProfiles = function(){
  var campaignsRef = Firebase.getRef("campaigns/")
  .orderByChild('status')
  .startAt('STATUS_VISIT')
  .endAt('STATUS_VISIT');
  campaignsRef.once("value", function(campaigns){
    campaigns.forEach(function(campaignItem){
      if(typeof campaignItem.val().lastVisit !== 'undefined'){ //visited one or more than one
        Bot.noFlood(campaignItem.key())
        .then(function(){
          Bot.getCredentials(campaignItem.val())
          .then(function(credentials){
            return Bot.visit(credentials, campaignItem.key());
          });
        });
      }else{ //not visited anyone yet
        Bot.getCredentials(campaignItem.val())
        .then(function(credentials){
          return Bot.visit(credentials, campaignItem.key());
        });
      }
    });
  });
};

process.on( 'SIGINT', function() {
  console.log( "\nGracefully shutting down from SIGINT (Ctrl-C)" );
  // some other closing procedures go here
  process.exit( );
});

interval(checkProfiles, 3000, 100000); //every 3 second needed to be restarted daily
