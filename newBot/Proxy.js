var Promise = require('promise');
var Firebase = require("./FirebaseRef");
// const fs = require('fs');
// var Firebase = require("./FirebaseRef");
//
// fs.readdir('./plugins', function (err, files) {
//   var plugin = require('./plugins/'+files[0]);
//   var getProxy = plugin.getProxy;
// });

var getProxy = function (campaignObj) {
  return new Promise(
    function(resolve, reject) {

      if(process.argv.indexOf("-useProxy") != -1){ //does our flag exist?
        var arg = process.argv[process.argv.indexOf("-useProxy") + 1]; //grab the next item
        if(arg === 'false'){
          resolve();
          return false;
        }
      }

      if(process.argv.indexOf("-customProxy") != -1){ //does our flag exist?
        customProxy = process.argv[process.argv.indexOf("-customProxy") + 1]; //grab the next item
        resolve(customProxy);
        return true;
      }

      var campaignsRef = Firebase.getRef("plugins/");
      campaignsRef.orderByChild("status").startAt("enabled").endAt("enabled").on("value", function(snapshot){
        var plugins = [];
        for (var property in snapshot.val()) {
          if (snapshot.val().hasOwnProperty(property)) {
            plugins.push(snapshot.val()[property].filename);
          }
        }

        //TODO use more than one plugin using async secuential calls to each enabled plugin
        var Proxy = require("./plugins/" + plugins[0]);
        Proxy.getProxy('ar').then(function(proxyString){
          resolve(proxyString);
        }).catch(function(error){
          console.log("Plugin File:"+ plugins[0]);
          console.log(error);
          reject(error);
        });

      });
    }
  );
};

module.exports = {
  getProxy: getProxy
};
