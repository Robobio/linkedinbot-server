var Firebase = require("./FirebaseRef");
var Promise = require('promise');
var striptags = require('striptags');
var Bot = require("./bot");

var interval = function (func, wait, times) {
    var interv = function (w, t) {
        return function () {
            if (typeof t === "undefined" || t-- > 0) {
                setTimeout(interv, w);
                try {
                    func.call(null);
                }
                catch (e) {
                    t = 0;
                    throw e.toString();
                }
            }
        };
    }(wait, times);
    setTimeout(interv, wait);
};

var checkCampaigns = function(){
  var campaignsRef = Firebase.getRef("campaigns/")
  .orderByChild('status')
  .startAt('STATUS_LIST')
  .endAt('STATUS_LIST');
  campaignsRef.once("value", function(campaigns){
    campaigns.forEach(function(campaignItem){
      var campaign = campaignItem.val();
      var campaignKey = campaignItem.key();
      var resultCount = campaignItem.val().resultCount;
      var profilesCount = 0;
      var profilesCountRef = Firebase.getRef("profiles/")
      .orderByChild('campaign')
      .startAt(campaignKey)
      .endAt(campaignKey);
      profilesCountRef.once("value", function(snapshot) {
        resultCount = parseFloat(resultCount.replace('.','').replace('.',''));
        //console.log(snapshot.numChildren());
        profilesCount = snapshot.numChildren();
        Bot.updateProfilesListCount(campaignKey, profilesCount);

        //console.log(profilesCount <= resultCount - 10);
        if(profilesCount <= resultCount - 10){
          var page = (profilesCount/10)+1;
          console.log("gettin page " + page + " of " + resultCount/10);
            Bot.getCommonCredentials()
            .then(function(credentials){
              return Bot.getPage(credentials, campaignItem.val(), campaignKey, page); })
                .then(function(links){
                  for (var i = 0; i < links.length; i++) {
                    console.log(links[i].name);
                  }
                  // console.log(links);
                  Bot.updateProfilesList(campaignKey, links);
            });
        }else{
          Bot.changeCampaignStatus(campaignKey, 'STATUS_VISIT');
        }
      });
    });
  });
};

interval(checkCampaigns, 5000, 20000); //Every 5 Second run for 1 day

process.on( 'SIGINT', function() {
  console.log( "\nGracefully shutting down from SIGINT (Ctrl-C)" );
  // some other closing procedures go here
  process.exit( );
});
