//Count the profiles for a given Campaign
var Firebase = require("./FirebaseRef");
var Bot = require("./bot");
var Promise = require('promise');
var campaignsRef = Firebase.getRef("campaigns/");
var Proxy = require('./Proxy');


var interval = function (func, wait, times) {
    var interv = function (w, t) {
        return function () {
            if (typeof t === "undefined" || t-- > 0) {
                setTimeout(interv, w);
                try {
                    func.call(null);
                }
                catch (e) {
                    t = 0;
                    throw e.toString();
                }
            }
        };
    }(wait, times);
    setTimeout(interv, wait);
};

var checkCampaignCounted = function (snapshot) {
  return (!(typeof snapshot.val().resultCount === 'undefined' || snapshot.val().resultCount === null));
};

var checkAndCount = function (snapshot) {
  if(!checkCampaignCounted(snapshot)){
    countProfilesForCampaign(snapshot);
  }
};

var countProfilesForCampaign = function (snapshot) {
  var credentialsObj = {};
  var campaignObj = snapshot;
  var proxyString = '';

  Bot.getCommonCredentials()
  .then(function (credentialsObj) {
    return Bot.countProfiles(credentialsObj, campaignObj.val(), campaignObj.key(), "");
  })
  .then(function (result) {
    console.log("Campaign: " + campaignObj.key());
    console.log(result);
  })
  .catch(function (erro) { console.log(erro); });
};

var checkZombieCampaigns = function(){
  var campaignsRef = Firebase.getRef("campaigns/");
  campaignsRef.orderByChild('status').startAt('notready').endAt('notready');
  campaignsRef.once("value", function(campaigns){
    campaigns.forEach(function(campaignItem){
      var date = new Date();
      if(date.getTime()+30000 > parseInt(campaignItem.val().created_at)){
        checkAndCount(campaignItem);
      }
    });
  });
};

campaignsRef.on("child_added", function(snapshot) {
  checkAndCount(snapshot);
});

campaignsRef.on("child_changed", function (childSnapshot, prevChildKey) {
  checkAndCount(childSnapshot);
});

interval(checkZombieCampaigns, 180000, 20000);

process.on( 'SIGINT', function() {
  console.log( "\nGracefully shutting down from SIGINT (Ctrl-C)" );
  // some other closing procedures go here
  process.exit( );
});
