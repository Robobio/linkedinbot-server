var Firebase = require("./FirebaseRef");

var watch = require('node-watch');
var fs = require('fs');
var screenShotRef = {};


watch('screenshots/', function(filename) {
  console.log("changed", filename);
  console.log(filename);
  var file = fs.readFileSync(filename);
  var b = new Buffer(file);
  var s = b.toString('base64');
  // console.log(decodeURI(s));
  console.log(filename.split("/")[1].split("#")[0]);
  screenShotRef = Firebase.getRef("/screenshots/"+ filename.split("/")[1].split("#")[0] +"/screenshot/");
  screenShotRef.set(s);
});

watch('profilescreenshots/', function(filename) {
  console.log("changed", filename);
  console.log(filename);
  var file = fs.readFileSync(filename);
  var b = new Buffer(file);
  var s = b.toString('base64');
  // console.log(decodeURI(s));
  console.log(filename.split("/")[1].split("#")[0]);
  screenShotRef = Firebase.getRef("/profiles/"+ filename.split("/")[1].split("#")[0] +"/screenshot/");
  screenShotRef.set(s);
});
