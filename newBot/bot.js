var Firebase = require("./FirebaseRef");
var Promise = require('promise');
var Nightmare = require("nightmare");
var winston = require('winston');
var exec = require('child_process').exec;


winston.add(winston.transports.File, { filename: 'logs/bot.log' });


var showBrowser = false;
if(process.argv.indexOf("-showBrowser") != -1){ //does our flag exist?
  showBrowser = process.argv[process.argv.indexOf("-showBrowser") + 1]; //grab the next item
  showBrowser = (showBrowser === 'true');
}

var useProxy = true;
if(process.argv.indexOf("-useProxy") != -1){ //does our flag exist?
  var arg = process.argv[process.argv.indexOf("-useProxy") + 1]; //grab the next item
  if(arg === 'false'){
    useProxy = false;
  }
}

var openDevTools = false;

var ONE_WEEK_TIME = 604800000;
var TWO_PER_MINUTE_TIME = 30000;

// My Bot Module
var init = function (snapshot) {
  return new Promise(
    function(resolve, reject) {
      this.snapshot = snapshot;
      resolve(snapshot);
    }
  );
};

var getCredentials = function (snapshot) {
  return new Promise(function(resolve, reject) {
    var credentials = Firebase.getRef("/credentials/"+snapshot.user+"/"+snapshot.credentials);
    credentials.once("value", function(credentialsSnapshot){
      resolve(credentialsSnapshot.val());
    });
  });
};

var getCommonCredentials = function () {
  return new Promise(function(resolve, reject) {
    var credentials = Firebase.getRef("/commoncredentials/");
    credentials.once("value", function(credentialsSnapshot){
      resolve(credentialsSnapshot.val());
    });
  });
};

var makeid = function(){
    var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

    for( var i=0; i < 5; i++ )
        text += possible.charAt(Math.floor(Math.random() * possible.length));

    return text;
};

var getNightmare = function(proxy, useProxy){
  var nightmare = {};
  if(proxy !== null && useProxy){
    nightmare = Nightmare({
      switches: {
        'proxy-server': proxy
      },
      webPreferences: {
        partition: 'lb'
      },
      show: showBrowser
    });
  }else{
    nightmare = Nightmare({
      show: showBrowser,
      webPreferences: {
        partition: 'lb'
      }
    });
  }
  return nightmare;
};

var saveRandomStamp = function(campaignKey){
  return new Promise(function(resolve, reject) {
    var resultCountRef = Firebase.getRef("/campaigns/"+campaignKey+"/random");
    resultCountRef.set(makeid);
    resolve();
  });
};

var saveCampaignResultCount = function (campaignKey, resultCount) {
  return new Promise(function(resolve, reject) {
    var resultCountRef = Firebase.getRef("/campaigns/"+campaignKey);
    resultCountRef.update({
      "profilesListedCount": 0,
      "resultCount": resultCount
    });
    changeCampaignStatus(campaignKey, "STATUS_LIST");
    //Save velocity
    var velocityRef = Firebase.getRef("/campaigns/"+campaignKey+"/velocity");
    var velocity = ONE_WEEK_TIME/parseInt(resultCount);
    if(velocity > TWO_PER_MINUTE_TIME){
      velocityRef.set(TWO_PER_MINUTE_TIME);
    }
    resolve(resultCount);
  });
};

var updateLastVisit = function(campaignKey){
  var statusRef = Firebase.getRef("/campaigns/"+campaignKey+"/lastVisit");
  var date = new Date();
  statusRef.set(date.getTime());
};

var updateProfilesListCount = function(campaignKey, count){
  var statusRef = Firebase.getRef("/campaigns/"+campaignKey+"/profilesListedCount");
  statusRef.set(count);
};

var changeCampaignStatus = function(campaignKey, newStatus){
  var statusRef = Firebase.getRef("/campaigns/"+campaignKey+"/status");
  statusRef.set(newStatus);
};

var getProxyForCampaign = function getProxyForCampaign (snapshot) {
  return new Promise(
    function(resolve, reject) {
      resolve(true);
    }
  );
};

var emailChallenge = function(){
  // #verification-code
  // #btn-primary
};

var monitor = function(key){
  winston.log('info', 'Guardando captura para la campania'+key);
  return function(nightmare) {
    nightmare
      .screenshot('screenshots/'+key+"#snapshot.png");
  };
};

var saveProfileCapture = function(key){
  return function(nightmare) {
    nightmare
      .screenshot('profilescreenshots/'+key+"#snapshot.png");
  };
};

var fillAdvancedSearchForm = function(snapshot,key){
      winston.log('info', "Llenando formulario de busqueda");
  return function(nightmare) {
    nightmare
      .goto("https://www.linkedin.com/vsearch/f?adv=true")
      .wait("#advs-keywords")
      .type("#advs-keywords",snapshot.keywords)
      .type("#advs-firstName", snapshot.firstName)
      .type("#advs-lastName", snapshot.lastName)
      .type("#advs-title", snapshot.title)
      .type("#advs-company", snapshot.company)
      .type("#advs-school", snapshot.school)
      .select("#advs-locationType", snapshot.locationType)
      .select("#advs-countryCode", snapshot.countryCode)
      .select("#advs-postalCode", snapshot.postalCode)
      .use(checkbox("#adv-F-N-ffs", snapshot.relation1))
      .use(checkbox("#adv-S-N-ffs", snapshot.relation2))
      .use(checkbox("#adv-A-N-ffs", snapshot.relation3))
      .use(checkbox("#adv-O-N-ffs", snapshot.relation4))
      .wait(1000)
      .use(monitor(key))
      .click("#peopleSearchForm > div.form-controls > input.submit-advs");
  };
};

var checkbox = function(selector, variable){
  return function(nightmare) {
    if (variable) nightmare.check(selector);
  };
};

var loginToLinkedin = function(key, credentials){
      winston.log('info', 'Iniciando login a Linkedin');
      return function(nightmare) {
            nightmare
                  .goto('https://www.linkedin.com')
                  .use(monitor(key))
                  .wait(5000)
                  .wait('#login-email')
                  .use(monitor(key))
                  .click('#login-email')
                  .type('#login-email', false)
                  .type('#login-email', credentials[Object.keys(credentials)[0]].email)
                  .type('#login-password', credentials[Object.keys(credentials)[0]].password)
                  .use(monitor(key))
                  .click('#login-submit')
                  .wait(2000)
                  .use(hasChallenge(credentials[Object.keys(credentials)[0]].challenge))
                  .wait("#main-site-nav > ul")
                  .use(monitor(key))
                  .wait(2000);
      };
};

var hasChallenge = function(challenge){
  return function(nightmare) {
    nightmare
      .wait(5000)
      .evaluate(function(){
        return document.querySelector("#verification-code");
      })
      .then(function(result){
        if(typeof result != "undefined"){
          nightmare
            .type("#verification-code", false)
            .type("#verification-code", "1234")
            .click("#btn-primary")
            .wait(5000);
        }
      });
  };
};

var countProfiles = function (credentials, snapshot, key, proxy) {
      winston.log('info', "Counting profiles", proxy, " using proxys", useProxy);
      return new Promise(function(resolve, reject) {
            useProxy = false;
            nightmare = getNightmare(proxy, useProxy);
            winston.log('info', "Iniciando Busqueda: "+snapshot.keywords);
            winston.log('info', "nightmare obj: "+nightmare);
            nightmare
                  .use(loginToLinkedin(key, credentials))
                  .use(fillAdvancedSearchForm(snapshot, key))
                  .wait(3000)
                  .evaluate(function () {
                        return document.querySelector("#results_count > div strong").innerHTML;
                  })
                  .end()
                  .then(function (result) {
                      winston.log('info', "Ended..."+ result);
                      saveCampaignResultCount(key, result)
                        .then(function(result){ resolve(result); });
                  })
                  .catch(function (error) {
                    winston.log('error', error);
                        saveRandomStamp()
                  .then(function(){ reject("Profile count failed:", error); });
                  });
      });
};

var getPage = function (credentials, snapshot, key, page, proxy) {
  return new Promise(function(resolve, reject) {
    changeCampaignStatus(key, "STATUS_LIST_busy");
    nightmare = getNightmare(proxy, useProxy);
    nightmare
      .use(loginToLinkedin(key, credentials))
      .use(fillAdvancedSearchForm(snapshot, key))
      .wait(3000)
      .wait("#results-col")
      .goto("https://www.linkedin.com/vsearch/p?keywords=" + snapshot.keywords + "&locationType=Y&page_num=" + page)
      .wait(3000)
      .use(monitor(key))
      .evaluate(function () {
          var links = [];
          var link = "";
          var name = "";
          var results = document.querySelectorAll("#results > li.mod.result.people");
          for (var i = 0; i < results.length; i++) {
            link = results[i].querySelector('a').href;
            name = results[i].querySelector('h3 a').innerHTML;
            if (typeof link == "string") {
                links.push({"name": name, "link": link});
            }
          }
          return links;
      })
      .end()
      .then(function (result) {
          changeCampaignStatus(key, "STATUS_LIST");
          resolve(result);
      })
      .catch(function (error) {
          changeCampaignStatus(key, "STATUS_LIST");
          console.error("Search failed:", error);
      });
    });
  };

  var updateProfilesList = function (campaignId, list) {
    var profilesRef = Firebase.getRef("/profiles/");
    var i = 0;
    for (i = 0; i < list.length; i++) {
      list[i].campaign = campaignId;
      list[i].visit = "queued";
      list[i].visit_campaign = "queued_" + campaignId;
      // winston.log('info', list[i]);
      profilesRef.push(list[i]);
    }
  };

var noFlood = function(campaignKey){
  return new Promise(function(resolve, reject) {
    var campaignRef = Firebase.getRef("/campaigns/"+campaignKey);
    campaignRef.on("value", function(campaignSnapshot){
      if(typeof campaignSnapshot.val().lastVisit == 'undefined'){
        resolve();
      }else{
        var date = new Date();
        if(date.getTime() > parseInt(campaignSnapshot.val().lastVisit) + parseInt(campaignSnapshot.val().velocity) ){
          resolve();
        }else{
          reject();
        }
      }
    });
  });
};

var profilevisited = function(profileKey, campaignKey){
  var statusRef = Firebase.getRef("/profiles/"+profileKey);
  statusRef.update({
    "visit": "visited",
    "visit_campaign": "visited_" + campaignKey
  });
  incrementVisitCount(campaignKey);
};

var incrementVisitCount = function(campaignKey){
  var visitCountRef = Firebase.getRef("/campaigns/"+campaignKey+"/visitedCount");
  visitCountRef.once("value", function(visitedCountSnapshot){
    var vc = visitedCountSnapshot.val();
    winston.log('info', visitedCountSnapshot.val());
    var visitIncrementRef = Firebase.getRef("/campaigns/"+campaignKey);
    visitIncrementRef.update({"visitedCount":++vc});
  });
};

var visit = function(credentials, campaignKey, proxy){
  var profileRef = Firebase.getRef("profiles/")
  .orderByChild('visit_campaign')
  .startAt('queued_' + campaignKey)
  .endAt('queued_' + campaignKey);
  profileRef.limitToFirst(1).once("value", function(profileSnapshot){
    if(profileSnapshot.val() !== null){
      var profileKey = Object.keys(profileSnapshot.val())[0];
      var campaign = profileSnapshot.val()[Object.keys(profileSnapshot.val())[0]];
      changeCampaignStatus(campaign.campaign, "STATUS_VISIT_busy");
      updateLastVisit(campaign.campaign);
      useProxy = false;
      nightmare = getNightmare(proxy, useProxy);
      nightmare
      .use(loginToLinkedin(campaignKey, credentials))
      .goto(campaign.link)
      .wait(5000)
      .use(saveProfileCapture(profileKey))
      .end()
      .then(function (result) {
        profilevisited(Object.keys(profileSnapshot.val())[0], campaignKey);
        changeCampaignStatus(campaign.campaign, "STATUS_VISIT");
      })
      .catch(function (error) {
        console.error("Search failed:", error);
      });
    }else{
      changeCampaignStatus(campaignKey, "STATUS_CHECK");
    }
  });
};

module.exports = {
  init: init,
  getCredentials : getCredentials,
  getCommonCredentials: getCommonCredentials,
  getProxyForCampaign : getProxyForCampaign,
  countProfiles : countProfiles,
  getPage : getPage,
  updateProfilesList : updateProfilesList,
  updateProfilesListCount: updateProfilesListCount,
  changeCampaignStatus : changeCampaignStatus,
  noFlood : noFlood,
  visit : visit
};
