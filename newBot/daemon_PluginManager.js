var Firebase = require("./FirebaseRef");
var fs = require('fs');
var files = [];
var databaseItems = [];

var campaignsRef = Firebase.getRef("proxyConfig/");

campaignsRef.on("child_changed", function(snapshot) {
  if(snapshot.val() == "refreshProxy"){

    var pluginsRef = Firebase.getRef("plugins/");

    pluginsRef.on("value", function(snapshot){

      fs.readdir('./plugins', function (err, f) {
        files = f;

        snapshot.forEach(function(item){
          databaseItems.push(item);
          var found = false;
          for (var i = 0; i < files.length; i++) {
            if(files[i] == item.val().filename){
              found = true;
            }
          }
          if(!found){
            var pluginFileRef = Firebase.getRef("plugins/"+item.key());
            pluginFileRef.remove();
          }
        });

        for (var j = 0; j < files.length; j++) {
          found = false;
          for (var i = 0; i < databaseItems.length; i++) {
            if(files[j] == databaseItems[i].val().filename){
              found = true;
            }
          }
          if(!found){
            var newObj = {
              "filename": files[j],
              "status": "available"
            };
            console.log(files[j].replace(".","-").replace(".","-"));
            var newPluginFileRef = Firebase.getRef("plugins/"+files[j].replace(".","-").replace(".","-"));
            newPluginFileRef.set(newObj);
          }
        }
      });

    });

    var proxyConfigRef = Firebase.getRef("proxyConfig/refreshProxy");
    proxyConfigRef.set("false");

  }
});
