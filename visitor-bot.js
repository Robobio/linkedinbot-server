var Firebase = require("firebase");


var interval = function (func, wait, times) {
    var interv = function (w, t) {
        return function () {
            if (typeof t === "undefined" || t-- > 0) {
                setTimeout(interv, w);
                try {
                    func.call(null);
                }
                catch (e) {
                    t = 0;
                    throw e.toString();
                }
            }
        };
    }(wait, times);
    setTimeout(interv, wait);
};

var getRef = function(node){
  var ref = new Firebase("https://linkedin-bot.firebaseio.com/" + node);
  ref.authWithCustomToken("TyINgeOuMWCVQdICcJY2f03EIbrJHwer1JnJ24lY");
  return ref;
};

var filterBy = function(ref, field, value){
  return ref.orderByChild(field).startAt(value).endAt(value);
};

var visitProfile = function(queuedProfileKey, queuedProfile){
  getRef("profiles/"+queuedProfileKey).update({
    'visit_campaign': "visiting_"+queuedProfile.campaign,
    "visit": "visiting"
  });
  console.log("Im gonna", queuedProfile);
};

var updateCampaignStatusAndVisitProfile = function(queuedProfileKey, queuedProfile){
  getRef("campaigns/"+queuedProfile.campaign).update({
    'lastVisit': Math.floor(new Date()),
    'visitingStatus': 'visiting'
  });
  visitProfile(queuedProfileKey, queuedProfile);
};

var getQueuedProfile = function(campaign){
  var profileRef = getRef("/profiles/");
  filterBy(profileRef, "visit_campaign", "queued_"+campaign.key()).once("value", function(snapshot) {
    var queuedProfile = snapshot.val()[Object.keys(snapshot.val())[0]];
    var queuedProfileKey = Object.keys(snapshot.val())[0];
    updateCampaignStatusAndVisitProfile(queuedProfileKey, queuedProfile);
  });
};

var searchVisitableProfiles = function(snapshot){
  snapshot.forEach(function(campaign){
    if(campaign.val().visitingconnections === 'yes'){
      getQueuedProfile(campaign);
    }
  });
};

var checkCampaigns = function () {
  var campaignsRef = getRef("/campaigns/");
  filterBy(campaignsRef, "visitingStatus", "ready").on("value", function(snapshot) {
    searchVisitableProfiles(snapshot);
  });
};

interval(checkCampaigns, 5000, 100);
