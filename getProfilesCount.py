import cookielib
import os
import urllib
import urllib2
import re
import string
import json
import sys
import cgi
from pprint import pprint
from BeautifulSoup import BeautifulSoup
from random import randint
import HTMLParser

username = sys.argv[1]
password = sys.argv[2]
searchOptions = sys.argv[3]
proxyIp = sys.argv[4]




cookie_filename = "parser.cookies.txt"

class LinkedInParser(object):

    def __init__(self, login, password, searchOptions, proxyIp):

        """ Start up... """
        self.login = login
        self.password = password
        #self.proxyIp = proxyIp
        self.searchOptions = json.loads(searchOptions)

        self.html_escape_table = {
          "&": "&amp;",
          '"': "&quot;",
          "'": "&apos;",
          ">": "&gt;",
          " ": "&20",
          "<": "&lt;",
        }

        # Simulate browser with cookies enabled
        self.cj = cookielib.MozillaCookieJar(cookie_filename)
        if os.access(cookie_filename, os.F_OK):
            self.cj.load()
        self.opener = urllib2.build_opener(
            urllib2.HTTPRedirectHandler(),
            urllib2.HTTPHandler(debuglevel=0),
            urllib2.HTTPSHandler(debuglevel=0),
            urllib2.HTTPCookieProcessor(self.cj),
            urllib2.ProxyHandler({'https': 'https://'+proxyIp})
        )
        self.opener.addheaders = [
            ('User-agent', ('Mozilla/4.0 (compatible; MSIE 6.0; '
                           'Windows NT 5.2; .NET CLR 1.1.4322)'))
        ]

        # Login
        self.loginPage()

        title = self.loadTitle()

        search_data = urllib.urlencode({
            'keywords': self.searchOptions['keywords'],
            'company': self.searchOptions['company'],
            'countryCode': self.searchOptions['countryCode'],
            'distance': self.searchOptions['distance'],
            'firstName': self.searchOptions['firstName'],
            'lastName': self.searchOptions['lastName'],
            'title': self.searchOptions['title'],
            'titleScope': self.searchOptions['titleScope'],
            'openAdvancedForm': 'Y',
            'locationType': self.searchOptions['locationType'],
            'postalCode': self.searchOptions['postalCode'],
            'school': self.searchOptions['school'],
            'titleScope': self.searchOptions['titleScope'],
            'rsid': '',
            'orig': 'ADVS',
            'rnd': str(randint(1, 9999))
        })

        result = self.loadPage("https://www.linkedin.com/vsearch/pj", search_data)


        print result
        result = json.loads(result)
        print result["content"]["page"]["voltron_unified_search_json"]["search"]["baseData"]["resultCount"]

        sys.exit
        #data = result.json()
        #pprint(data["content"]["page"]["voltron_unified_search_json"]["search"]["results"])
        #self.cj.save()




    def html_escape(self, text):
      """
      Produce entities within text.
      """
      return "".join(self.html_escape_table.get(c,c) for c in text)


    def getSearchOptionsValue(self, value):
      if(self.searchOptions[value] == ''):
        return ''
      else:
        return "&" + value + "=" + self.html_escape(self.searchOptions[value].strip())


    def loadPage(self, url, data=None):
        """
        Utility function to load HTML from URLs for us with hack to continue despite 404
        """
        # We'll print the url in case of infinite loop
        # print "Loading URL: %s" % url
        try:
            if data is not None:
                response = self.opener.open(url, data)
            else:
                response = self.opener.open(url)
            return ''.join(response.readlines())
        except:
            # If URL doesn't load for ANY reason, try again...
            # Quick and dirty solution for 404 returns because of network problems
            # However, this could infinite loop if there's an actual problem
            print "Could not get " + url
            #return self.loadPage(url, data)
            return False

    def loginPage(self):
        """
        Handle login. This should populate our cookie jar.
        """
        html = self.loadPage("https://www.linkedin.com/")
        soup = BeautifulSoup(html)
        csrf = soup.find(id="loginCsrfParam-login")['value']

        login_data = urllib.urlencode({
            'session_key': self.login,
            'session_password': self.password,
            'loginCsrfParam': csrf,
        })

        html = self.loadPage("https://www.linkedin.com/uas/login-submit", login_data)
        return

    def loadTitle(self):
        html = self.loadPage("http://www.linkedin.com/nhome")
        soup = BeautifulSoup(html)
        return soup.find("title")

parser = LinkedInParser(username, password, searchOptions, proxyIp)
