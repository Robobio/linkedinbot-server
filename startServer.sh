echo "┬─┐┌─┐┌┐ ┌─┐┌┐  ┬┌─┐";
echo "├┬┘│ │├┴┐│ │├┴┐ ││ │";
echo "┴└─└─┘└─┘└─┘└─┘o┴└─┘";

echo "Shutting down all previous scripts"
forever stopall;

echo "Starting Counter Script"
forever start newBot/profileCountService.js
echo "Starting Lister Script"
forever start newBot/profileListService.js
echo "Starting Visiter Script"
forever start newBot/profileVisitService.js
#echo "Starting VisitBack Script"
#forever start newBot/profileVisitBackService.js
