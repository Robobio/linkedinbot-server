var Firebase = require("firebase");
var pc = require("proxy-checker");
var ProxyLists = require('proxy-lists');
var ipLocation = require('ip-location');

var saveProxy = function(proxyData, cb){
  var FURL = "https://linkedin-bot.firebaseio.com";
  proxyData.country = proxyData.country.toUpperCase();
  //console.log(proxyData);
  proxyId = proxyData.ipPort.replace(".", "_").replace(".", "_").replace(".", "_").replace(".", "_").replace(":", "-");
  var proxyRef = new Firebase(FURL + "/proxys/" + proxyId);
  proxyRef.authWithCustomToken("TyINgeOuMWCVQdICcJY2f03EIbrJHwer1JnJ24lY");
  proxyRef.transaction(function(currentData) {
    if (currentData === null) {
      proxyRef.set(proxyData);
    } else {
      //console.log('User wilma already exists.');
      return; // Abort the transaction.
    }
  }, function(error, committed, snapshot) {

    if (error) {
      //console.log('Transaction failed abnormally!', error);
    }else if (!committed){
      //console.log('We aborted the transaction (record already exists).');
      //console.log('Wilma\'s data: ', proxyData);
      cb(proxyData);
    }else{
      //console.log('Proxy added!');
      //console.log('Proxy data: ', proxyData);
      cb(proxyData);
    }

  });

};

var getProxyFromDatabase = function(status_country_campaign, cb){
  var ref = new Firebase("https://linkedin-bot.firebaseio.com/proxys");
  ref.authWithCustomToken("TyINgeOuMWCVQdICcJY2f03EIbrJHwer1JnJ24lY");
  ref.orderByChild("status_country_campaign").startAt(status_country_campaign).endAt(status_country_campaign).limitToFirst(1).once("value", function(data) {
    if(data.numChildren() === 0){
      cb(false);
    }else{
      var first;
      for (var i in data.val()) {
          if (data.val().hasOwnProperty(i) && typeof(i) !== 'function') {
              first = data.val()[i];
              break;
          }
      }
      cb(first);
    }
  });
};

var gimmeProxy = function(country, cb){
  var Client = require('node-rest-client').Client;

  var client = new Client();
  var url = "http://gimmeproxy.com/api/getProxy?anonymityLevel=1&supportsHttps=true&country="+country;
  client.registerMethod("getMethod", url , "GET");

  var args = {
    headers: { "Content-Type": "application/json" }
  };

  client.methods.getMethod(args, function (data, response) {
    if(Buffer.isBuffer(data)){
      data = data.toString('utf8');
    }
    try {
      JSON.parse(data);
      // parsed response body as js object
      if(data == '{"error" : "no more proxies left"}'){
        console.log("No proxies left");
        cb(false);
      }else{
        cb(data);
      }
    } catch (e) {
      //console.log(data.ipPort);
      if(data.ipPort){
        cb(data);
      }else{
        cb(false, e);
        console.log("not valid JSON", data);
      }
    }

    // raw response
  });

};

var ProxyListsGet = function(country, callback){
  var c = [];
  var p = [];

  c.push(country.toUpperCase());

  var options = {
      countries: c
  };

  // `gettingProxies` is an event emitter object.
  var gettingProxies = ProxyLists.getProxies(options);

  gettingProxies.on('data', function(proxies) {
      if(proxies.length > 0){
        for (var i = 0; i < proxies.length; i++) {
          console.log("Adding new proxy to the stack..." + proxies[i]);
          p.push(proxies[i]);
        }
      }
  });

  gettingProxies.on('error', function(error) {
      // Some error has occurred.
      console.error(error);
  });

  gettingProxies.once('end', function() {
    console.log(p);
    var callback = function(data){cb(data);};
    var proxyData = {};
    for (var i = 0; i < p.length; i++) {
      proxyData = {};
      proxyData.ip = p[i].ipAddress;
      proxyData.port = p[i].port;
      proxyData.ipPort = p[i].ipAddress + ":" + p[i].port;
      proxyData.country = p[i].country;
      proxyData.creationDate = Firebase.ServerValue.TIMESTAMP;
      proxyData.provider = p[i].source;
      if(i===0){
        proxyData.campaign = campaign;
        proxyData.status_country_campaign = "up_"+data.country + "_" + campaign;
      }else{
        proxyData.campaign = '';
        proxyData.country_campaign = "up_" + p[i].country + "_" + '';
      }
      saveProxy(proxyData, null);
    }
    saveProxy(proxyData, callback);
  });
};

var getProxy = function(country, campaign, cb){
  country = country.toUpperCase();
  console.log("Getting proxy for " + country);
  console.log("Trying to get this: " + "up_" + country);
  getProxyFromDatabase("up_" + country + "_", function(data){
    console.log("Getting proxy from database...");
    if(data === false){
      console.log("No proxy from database, getting proxy...");

      ProxyListsGet(country, cb);
      /*
      gimmeProxy(country, function(data){
          if(data!==false){
            var proxyData = {};
            proxyData.ip = data.ip;
            proxyData.port = data.port;
            proxyData.ipPort = data.ipPort;
            proxyData.country = data.country;
            proxyData.creationDate = Firebase.ServerValue.TIMESTAMP;
            proxyData.campaign = campaign;
            proxyData.provider = 'gimmeProxy';
            proxyData.status_country_campaign = "up_"+data.country + "_" + campaign;
            saveProxy(proxyData, function(data){
              cb(data);
            });
          }else{
            console.log("Could not get proxy, trying another service");
          }
      });
      */
    }else{
      console.log("Using Database proxy");
      cb(data);
    }
  });
};

var setProxyStatus = function(hostPort, status){
  if(typeof hostPort !== 'string' || hostPort === ''){ return false; }
  var proxyId = hostPort.replace(".", "_").replace(".", "_").replace(".", "_").replace(".", "_").replace(":", "-");

  var refCampaign = new Firebase("https://linkedin-bot.firebaseio.com/proxys/" + proxyId);
  refCampaign.authWithCustomToken("TyINgeOuMWCVQdICcJY2f03EIbrJHwer1JnJ24lY");
  refCampaign.once("value", function(refCampaignSnapshot){
    var campaignId = refCampaignSnapshot.val().campaign;
    var country = refCampaignSnapshot.val().country;
    var proxyStatusRef = new Firebase("https://linkedin-bot.firebaseio.com/proxys/" + proxyId + "/status");
    var proxyLastCheckRef = new Firebase("https://linkedin-bot.firebaseio.com/proxys/" + proxyId + "/lastcheck");
    var proxyStatusCountryCampaignRef = new Firebase("https://linkedin-bot.firebaseio.com/proxys/" + proxyId + "/status_country_campaign");
    proxyStatusRef.set(status);
    proxyLastCheckRef.set(Firebase.ServerValue.TIMESTAMP);
    proxyStatusCountryCampaignRef.set(status + "_" + country + "_" + campaignId);
  });
};

var checkProxys = function(hostPort, callback){
  console.log("Checking Proxy..."+hostPort);
  setProxyStatus(hostPort, 'checking');
  var arr = hostPort.split(":");
  pc.checkProxy(arr[0], arr[1], {
    // the complete URL to check the proxy
    url: 'http://www.example.com',
    // an optional regex to check for the presence of some text on the page
    regex: /Example Domain/
  }, function(host, port, ok, statusCode, err) {
    if (ok) {
      setProxyStatus(hostPort, 'up');
      console.log("Proxy OK");
    }else{
      setProxyStatus(hostPort, 'down');
      console.log("Proxy Failed");
    }
    callback(ok);
    //console.log(host + ':' + port + ' => ' + ok + ' (status: ' + statusCode + ', err: ' + err + ')');
  });
};

var locateProxy = function(hostPort, callback){
  var arr = hostPort.split(":");
  ipLocation(arr[0], function (err, data) {
    callback(data);
  });
};

module.exports = {
    getProxy: getProxy,
    checkProxys: checkProxys,
    locateProxy: locateProxy
};
