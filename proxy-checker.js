var Firebase = require("firebase");
var proxy = require("./proxy");


var interval = function (func, wait, times) {
    var interv = function (w, t) {
        return function () {
            if (typeof t === "undefined" || t-- > 0) {
                setTimeout(interv, w);
                try {
                    func.call(null);
                }
                catch (e) {
                    t = 0;
                    throw e.toString();
                }
            }
        };
    }(wait, times);
    setTimeout(interv, wait);
};

var getOneNotCheckedProxyToCheck = function (callback) {
    var ref = new Firebase("https://linkedin-bot.firebaseio.com/proxys");
    ref.authWithCustomToken("TyINgeOuMWCVQdICcJY2f03EIbrJHwer1JnJ24lY");
    ref.orderByChild("status").startAt("notchecked").endAt("notchecked").limitToFirst(1).once("value", function (data) {
        if (data.numChildren() === 1) {
            var first;
            for (var i in data.val()) {
                if (data.val().hasOwnProperty(i) && typeof(i) !== 'function') {
                    first = data.val()[i];
                    break;
                }
            }
            callback(first.ipPort);
        } else {
            callback(false);
        }

    });
};

var getOneNotLocatedProxyToCheck = function (callback) {
    var ref = new Firebase("https://linkedin-bot.firebaseio.com/proxys");
    ref.authWithCustomToken("TyINgeOuMWCVQdICcJY2f03EIbrJHwer1JnJ24lY");
    ref.orderByChild("status").startAt('locate').endAt('locate').limitToFirst(1).once("value", function (data) {
        if (data.numChildren() === 1) {
            var first;
            for (var i in data.val()) {
                if (data.val().hasOwnProperty(i) && typeof(i) !== 'function') {
                    first = data.val()[i];
                    break;
                }
            }
            callback(first.ipPort);
        } else {
            callback(false);
        }

    });
};

var setProxyStatus = function (hostPort, status) {
    if (typeof hostPort !== "string" || hostPort === '') {
        return false;
    }
    var proxyId = hostPort.replace(".", "_").replace(".", "_").replace(".", "_").replace(".", "_").replace(":", "-");

    //TODO:
    //guardar status_country_campaign cuando se setea el status
    var refCampaign = new Firebase("https://linkedin-bot.firebaseio.com/proxys/" + proxyId);
    refCampaign.authWithCustomToken("TyINgeOuMWCVQdICcJY2f03EIbrJHwer1JnJ24lY");
    refCampaign.once("value", function (refCampaignSnapshot) {
        var campaignId = refCampaignSnapshot.val().campaign;
        var country = refCampaignSnapshot.val().country;
        var proxyStatusRef = new Firebase("https://linkedin-bot.firebaseio.com/proxys/" + proxyId + "/status");
        var proxyLastCheckRef = new Firebase("https://linkedin-bot.firebaseio.com/proxys/" + proxyId + "/lastcheck");
        var proxyStatusCountryCampaignRef = new Firebase("https://linkedin-bot.firebaseio.com/proxys/" + proxyId + "/status_country_campaign");
        proxyStatusRef.set(status);
        proxyLastCheckRef.set(Firebase.ServerValue.TIMESTAMP);
        proxyStatusCountryCampaignRef.set(status + "_" + country + "_" + campaignId);
    });
};

var setProxyLocation = function (hostPort, location) {
    var proxyId = hostPort.replace(".", "_").replace(".", "_").replace(".", "_").replace(".", "_").replace(":", "-");
    var proxyLocationRef = new Firebase("https://linkedin-bot.firebaseio.com/proxys/" + proxyId + "/country");
    proxyLocationRef.set(location.toUpperCase());
};

var deactivateZombieChecks = function () {
    var ref = new Firebase("https://linkedin-bot.firebaseio.com/proxys");
    ref.authWithCustomToken("TyINgeOuMWCVQdICcJY2f03EIbrJHwer1JnJ24lY");
    ref.orderByChild("status").startAt('checking').endAt('checking').limitToFirst(1).once("value", function (zombies) {
        var offsetRef = new Firebase("https://linkedin-bot.firebaseio.com/.info/serverTimeOffset");
        offsetRef.on("value", function (snap) {
            var offset = snap.val();
            var estimatedServerTimeMs = new Date().getTime() + offset;
            zombies.forEach(function (zombie) {
                if ((estimatedServerTimeMs - zombie.val().lastcheck) > 120000) {
                    console.log("Killing zombie in " + zombie.key());
                    var refZombie = new Firebase("https://linkedin-bot.firebaseio.com/proxys/" + zombie.key());
                    refZombie.authWithCustomToken("TyINgeOuMWCVQdICcJY2f03EIbrJHwer1JnJ24lY");
                    refZombie.update({'lastcheck': Firebase.ServerValue.TIMESTAMP, 'status': 'down'});
                }
            });
        });
    });
};

var checkProxys = function () {
    deactivateZombieChecks();

    getOneNotCheckedProxyToCheck(function (hostPort) {
        if (hostPort === false) {
            return false;
        }
        setProxyStatus(hostPort, 'checking');
        var callback = function (checkResult) {
            if (checkResult) {
                setProxyStatus(hostPort, "up");
            } else {
                setProxyStatus(hostPort, "down");
            }
        };
        proxy.checkProxys(hostPort, callback);
    });

    getOneNotLocatedProxyToCheck(function (hostPort) {
        if (hostPort === false) {
            return false;
        }
        setProxyStatus(hostPort, 'locating');
        var callback = function (locationResult) {
            if (locationResult) {
                setProxyLocation(hostPort, locationResult.country_code);
                setProxyStatus(hostPort, "notchecked");
            }
        };
        proxy.locateProxy(hostPort, callback);
    });

};

interval(checkProxys, 1000, 100);
