var Firebase = require("firebase");
var PythonShell = require("python-shell");
var proxy = require("./proxy");
var Nightmare = require("nightmare");
var showBrowser = true;
var useProxy = false;
var openDevTools = true;

var getProfilesCount = function (options, callback) {
  var result = "";
  var username = options.args[0];
  var password = options.args[1];
  var search = JSON.parse(options.args[2]).keywords;
  var proxyServer = options.args[3];

  var nightmareOptions = {
    show: showBrowser
  };
  if(useProxy){
    nightmareOptions.switches =  {
      "proxy-server": proxyServer // set the proxy server here ...
    };
  }
  if(openDevTools){
    nightmareOptions.openDevTools =  {
       mode: 'detach'
    };
  }
  var nightmare = Nightmare(nightmareOptions);

  nightmare
    .goto("https://www.linkedin.com/uas/login?goback=&trk=hb_signin")
    .wait(5000)
    .wait("#session_key-login")
    .type("#session_key-login", false)
    .type("#session_key-login", username)
    .type("#session_password-login", password)
    .click("#btn-primary")
    .wait(5000)
    .wait("#main-site-nav > ul")
    .goto("https://www.linkedin.com/vsearch/f?adv=true")
    .wait(5000)
    .wait("#advs-keywords")
    .type("#advs-keywords",search)
    .click("#peopleSearchForm > div.form-controls > input.submit-advs")
    .wait(3000)
    .evaluate(function () {
      return document.querySelector("#results_count > div strong").innerHTML;
    })
    .end()
    .then(function (result) {
      callback(result);
    })
    .catch(function (error) {
      console.error("Search failed:", error);
    });
};

var getPage = function (options, page, callback) {
    var username = options.args[0];
    var password = options.args[1];
    var search = JSON.parse(options.args[2]).keywords;
    var proxyServer = options.args[3];

    var nightmareOptions = {
      show: showBrowser
    };
    if(useProxy){
      nightmareOptions.switches =  {
        "proxy-server": proxyServer // set the proxy server here ...
      };
    }
    if(openDevTools){
      nightmareOptions.openDevTools =  {
         mode: 'detach'
      };
    }
    var nightmare = Nightmare(nightmareOptions);

    nightmare
        .goto("https://www.linkedin.com/uas/login?goback=&trk=hb_signin")
        .wait(5000)
        .wait("#session_key-login")
        .type("#session_key-login", false)
        .type("#session_key-login", username)
        .type("#session_password-login", password)
        .click("#btn-primary")
        .wait("#main-site-nav > ul")
        .goto("https://www.linkedin.com/vsearch/f?adv=true")
        .wait(5000)
        .wait("#advs-keywords")
        .type("#advs-keywords", search)
        .click("#peopleSearchForm > div.form-controls > input.submit-advs")
        .wait(5000)
        .wait("#results-col")
        .goto("https://www.linkedin.com/vsearch/p?keywords=" + search + "&locationType=Y&page_num=" + page)
        .wait(5000)
        .evaluate(function () {
            var links = [];
            var link = "";
            var name = "";
            for (i = 0; i < 10; i += 1) {
                //#results > li.mod.result.idx1.    people > div > h3 > a
                //var link = document.querySelector('#results > li.mod.result.idx'+i+'.people > div > h3 > a').href;
                link = document.querySelector("#results > li.mod.result.idx" + i + ".people > div > h3 > a").href;
                name = document.querySelector("#results > li.mod.result.idx" + i + ".people > div > h3 > a").innerHTML;
                if (typeof link == "string") {
                    links.push({"name": name, "link": link});
                }
            }
            return links;
        })
        .end()
        .then(function (result) {
            callback(result);
        })
        .catch(function (error) {
            console.error("Search failed:", error);
        });
};

var startCampaign = function (snapshot, data, proxyData) {
  var options = {args: [data.username, data.password, JSON.stringify(snapshot.val()), proxyData.ipPort ]};
  console.log("Getting profiles for " + data.username);
  getProfilesCount(options, function(resultNumber){
    console.log("Profiles count:" + resultNumber);
    updateResultsCount(snapshot.key(), resultNumber);
    if(snapshot.val().visitingconnections == "yes"){
      console.log("Getting profiles list");
      addCampaignNotification(snapshot.key(), "Getting profiles list");
    }
  });
};

var prepare = function (snapshot, prepareCallback) {
  var options = {};
  getLinkedinCredentials(snapshot.val().user, function(data){
    data = data.val()[Object.keys(data.val())[0]];
    if(!useProxy){
      prepareCallback(snapshot, data, '127.0.0.1:8080');
    }else{
      //Get Profiles Count
      var getProxys = function(country, campaignId){
        addCampaignNotification(snapshot.key(), "Getting proxy for country "+data.country+"...");
        proxy.getProxy(data.country, snapshot.key(), function(proxyData){
          var callback = function(result){
            if(!result){ //Proxy check not ok
              addCampaignNotification(snapshot.key(), "Proxy check Failed" );
              getProxys(data.country, snapshot.key(), callback);
              return;
            }
            //proxy check ok
            if(proxyData===false){
              addCampaignNotification(snapshot.key(), "Could not get proxy for country" + data.country );
              getProxys(data.country, snapshot.key(), callback);
              return;
            }
            prepareCallback(snapshot, data, proxyData);
          };
          proxy.checkProxys(proxyData.ipPort, callback);
        });
      };
      getProxys(data.country, snapshot.key());
    }
  });
};

var addCampaignNotification = function (id, message) {
  var FURL = "https://linkedin-bot.firebaseio.com";
  var campaignRef = new Firebase(FURL + "/campaigns/" + id + "/notification/");
  campaignRef.authWithCustomToken("TyINgeOuMWCVQdICcJY2f03EIbrJHwer1JnJ24lY");
  campaignRef.set(message);
};

var getLinkedinCredentials = function (uid, cb) {
  var FURL = "https://linkedin-bot.firebaseio.com";
  var campaignRef = new Firebase(FURL + "/credentials/"+uid);
  campaignRef.authWithCustomToken("TyINgeOuMWCVQdICcJY2f03EIbrJHwer1JnJ24lY");
  //campaignRef .orderByChild('uid').startAt(uid).endAt(uid);
  campaignRef.on("value", cb);
};

var updateProfilesList = function (id, list) {
  var FURL = "https://linkedin-bot.firebaseio.com";
  var campaignRef = new Firebase(FURL + "/profiles/");
  campaignRef.authWithCustomToken("TyINgeOuMWCVQdICcJY2f03EIbrJHwer1JnJ24lY");
  var i = 0;
  for (i = 0; i < list.length; i++) {
    list[i].campaign = id;
    list[i].visit = "queued";
    list[i].visit_campaign = "queued_" + id;
    //console.log(list[i]);
    campaignRef.push(list[i]);
  }

};

var updateResultsCount = function (id, count) {
  var FURL = "https://linkedin-bot.firebaseio.com";
  var campaignRef = new Firebase(FURL + "/campaigns/"+ id);
  campaignRef.authWithCustomToken("TyINgeOuMWCVQdICcJY2f03EIbrJHwer1JnJ24lY");
  campaignRef.once("value", function(snapshot){

    var velocity = snapshot.val().duration / parseInt(count.split('.').join(""));
    velocity = Math.floor(velocity);
    campaignRef.child("velocity").set(velocity);

  });
  campaignRef.child("resultCount").set(count);
};

var getProfiles = function (snapshot, data, proxyData) {
  var options = {args: [data.username, data.password, JSON.stringify(snapshot.val()), proxyData.ipPort ]};
  var profiles = [];
  getPage(options, 1, function(resultProfiles){
    console.log(resultProfiles);
    updateProfilesList(snapshot.key(), resultProfiles);
  });
};

var FURL = "https://linkedin-bot.firebaseio.com/";
var campaignsRef = new Firebase(FURL + "/campaigns/");
campaignsRef.authWithCustomToken("TyINgeOuMWCVQdICcJY2f03EIbrJHwer1JnJ24lY");

campaignsRef.on("child_added", function(snapshot) {
  if(snapshot.val().resultCount === undefined){
    prepare(snapshot, function(snapshot, data, proxyData){
      startCampaign(snapshot, data, proxyData);
    });
  }
});

campaignsRef.on("child_changed", function (childSnapshot, prevChildKey) {

  var daemonStatus = "";

  if(typeof childSnapshot.val().resultCount !== "undefined"){
    console.log("Need to try to get profiles list");
    prepare(childSnapshot, function(snapshot, data, proxyData){
      getProfiles(snapshot, data, proxyData);
    });
  }

  if(childSnapshot.val().status === "ready"){
    console.log("Visit Profile");
    prepare(childSnapshot, function(snapshot, data, proxyData){
      console.log("Visit One Profile");
    });
  }

});
