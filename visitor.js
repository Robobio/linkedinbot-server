var Firebase = require("firebase");
var PythonShell = require('python-shell');
var proxy = require("./proxy");
var Nightmare = require('nightmare');


var getCampaign = function(id, cb){
  var FURL = "https://linkedin-bot.firebaseio.com";
  var campaignRef = new Firebase(FURL + "/campaigns/"+id);
  campaignRef.authWithCustomToken("TyINgeOuMWCVQdICcJY2f03EIbrJHwer1JnJ24lY");
  //campaignRef .orderByChild('uid').startAt(uid).endAt(uid);
  campaignRef.on('value', cb);
};

var getLinkedinCredentials = function(uid, cb){
  var FURL = "https://linkedin-bot.firebaseio.com";
  var campaignRef = new Firebase(FURL + "/credentials/"+uid);
  campaignRef.authWithCustomToken("TyINgeOuMWCVQdICcJY2f03EIbrJHwer1JnJ24lY");
  //campaignRef .orderByChild('uid').startAt(uid).endAt(uid);
  campaignRef.on('value', cb);
};

var getProfile = function(id, cb){
  var FURL = "https://linkedin-bot.firebaseio.com";
  var campaignRef = new Firebase(FURL + "/profiles/"+uid);
  campaignRef.authWithCustomToken("TyINgeOuMWCVQdICcJY2f03EIbrJHwer1JnJ24lY");
  //campaignRef .orderByChild('uid').startAt(uid).endAt(uid);
  campaignRef.on('value', cb);
};

var visitProfile = function(id, callback){

  var username = options.args[0];
  var password = options.args[1];
  var search = JSON.parse(options.args[2]).keywords;
  var proxy = options.args[3];

  var Nightmare = require('nightmare');
  var nightmare = Nightmare({
    switches: {
      'proxy-server': proxy // set the proxy server here ...
    },
    show: true
  });
  nightmare
    .goto('https://www.linkedin.com/uas/login?goback=&trk=hb_signin')
    .type('#session_key-login', false)
    .type('#session_key-login', username)
    .type('#session_password-login', password)
    .click('#btn-primary')
    .wait('#main-site-nav > ul')


    .click('#advanced-search')
    .wait(3000)
    .type('#advs-keywords', search)
    .click('#peopleSearchForm > div.form-controls > input.submit-advs')
    .wait('#results-col')
    .wait(3000)
    .goto('https://www.linkedin.com/vsearch/p?keywords='+search+'&locationType=Y&page_num='+page)
    .wait(3000)
    .evaluate(function () {
      var links = [];
      for (var i = 0; i < 10; i++) {
                                           //#results > li.mod.result.idx1.    people > div > h3 > a
        //var link = document.querySelector('#results > li.mod.result.idx'+i+'.people > div > h3 > a').href;
        var link = document.querySelector('#results > li.mod.result.idx'+i+'.people > div > h3 > a').href;
        var name = document.querySelector('#results > li.mod.result.idx'+i+'.people > div > h3 > a').innerHTML;
        if(typeof link == 'string'){
          links.push({'name':name, 'link':link});
        }
      }
      return links;
    })
    .end()
    .then(function (result) {
      callback(result);
    })
    .catch(function (error) {
      console.error('Search failed:', error);
    });
};
