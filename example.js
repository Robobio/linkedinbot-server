
var getResultCount = function(username, password, search, page, proxy, callback){
  var Nightmare = require('nightmare');
  var nightmare = Nightmare({
    switches: {
      'proxy-server': proxy // set the proxy server here ...
    },
    show: false
  });
  nightmare
    .goto('https://www.linkedin.com/uas/login?goback=&trk=hb_signin')
    .type('#session_key-login', false)
    .type('#session_key-login', username)
    .type('#session_password-login', password)
    .click('#btn-primary')
    .wait('#main-site-nav > ul')
    .click('#advanced-search')
    .wait(5000)
    .type('#advs-keywords',search)
    .click('#peopleSearchForm > div.form-controls > input.submit-advs')
    .wait('#results-col')
    .wait(5000)
    .goto('https://www.linkedin.com/vsearch/p?keywords=swift&locationType=Y&page_num='+page)
    .wait(10000)
    .evaluate(function () {
      var links = [];
      for (var i = 0; i < 10; i++) {
        links.push(document.querySelector('#results > li.mod.result.idx'+i+'.people > div > h3 > a').href);
      }
      return links;
    })
    .end()
    .then(function (result) {
      callback(result);
    })
    .catch(function (error) {
      console.error('Search failed:', error);
    });
};

getResultCount('juanpedro@robob.io', 'juanpedro123456', 'php codeigniter mysql', '190.15.200.31:8080', function(result){
  console.log(result);
});
